import InvoiceLine from "./invoiceLine";

class Invoice {
  invoiceDate: Date;
  invoiceNumber: string;
  lineItems: InvoiceLine[];

  constructor(invoiceDate = new Date(), invoiceNumber = "", lineItems: Array<InvoiceLine> = []) {
    this.invoiceDate = invoiceDate;
    this.invoiceNumber = invoiceNumber;
    this.lineItems = lineItems;
  }

  /**
   * Adds a line to the invoice
   */
  addInvoiceLine(line: InvoiceLine): void {
    this.lineItems.push(line);
  };

  /**
   * Removes a line
   */
  removeInvoiceLine(id: number): void {
    const index = this.lineItems.findIndex(l => l.invoiceLineId === id)
    if (index !== -1) {
      this.lineItems.splice(index, 1)
    }
  };

  /**
   * Get total of invoice lines
   */
  getTotal(): number {
    return this.lineItems.reduce((acc, invoiceLine) => invoiceLine.quantity * invoiceLine.cost + acc, 0);
  };

  /**
   * Merge invoice with current invoice
   * @param invoice invoice to merge
   */
  mergeInvoices(invoice: Invoice): void {
    invoice.lineItems.forEach(lineItem => {
      this.lineItems.push(lineItem);
    })
  }

  clone(): Invoice {
    return new Invoice(new Date(), this.invoiceNumber, this.lineItems);
  };

  toString(): string {
    return `Invoice ${this.invoiceNumber} Date: ${this.invoiceDate.toISOString()} with ${this.lineItems.length} items. Total: ${this.getTotal()}`;
  }
}

export default Invoice;
