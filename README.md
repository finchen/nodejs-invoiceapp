# Technical Test

## Instructions

  ---------------------------------------------------------------------------------
    The test consists of a small invoice application that has a number of issues.
    Your job is to fix them and make sure you can perform the functions in each method below.
    Note your first job is to get the solution to execute! 
	
    Rules
    ---------------------------------------------------------------------------------
    * The entire solution must be written in Javascript or TypeScript.
    * Feel free to use ECMA2015 (ES6) syntax
    * You can modify any of the code in this solution, split out classes etc
    * You can modify Invoice and InvoiceLine, rename and add methods, change property types (hint) 
    * Feel free to use any libraries or frameworks you like
    * Feel free to write tests (hint) 
    * Show off your skills! 

Original files are in [./original](./original) folder.


## Solution

I have chosen to use node and typescript.

First, I have typed the model class `Invoice` and `InvoiceLine` and tested their instance creation.

Then, I used the original `index.js` to create a set of test.

I developed the invoice functionalities with the unit test: `npm run test:watch` but the original script (now in `index.ts`) can still be ran with `npm run start:watch`.

I have spent just less than 2 hours.

- Typescript
- Type definitions for Node.js and Jest
- Tools to enforce styling: prettier, eslint, .editorconfig
- Unit test with jest

### Source code

Refactored and fixed in `src/`

### Tests

Based from `index.js` in `tests/`

### Commands

- `npm install` - install required packages
- `npm run test` - run unit test
- `npm run build` - transpile typescript to es2019 into `dist/`
- `npm start` - run compiled code from `dist/`
- `npm run start:watch` - compile and run in watch mode
