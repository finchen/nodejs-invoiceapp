import InvoiceLine from "../src/models/invoiceLine";

describe("invoice line class", () => {

  it("should create an invoice line", () => {
    const invoiceLine = new InvoiceLine(1, 6.99, 1, "Apple");
    expect(invoiceLine.invoiceLineId).toBe(1);
    expect(invoiceLine.cost).toBe(6.99);
    expect(invoiceLine.quantity).toBe(1);
    expect(invoiceLine.description).toBe("Apple");
  });

  it("should change the description", () => {
    const invoiceLine = new InvoiceLine(1, 6.99, 1, "Apple");
    invoiceLine.description = "Rotten Apple";
    expect(invoiceLine.description).toBe("Rotten Apple");
  });

});
