import Invoice from "../src/models/Invoice";
import InvoiceLine from "../src/models/invoiceLine";

describe("invoice functionality", () => {

  it("should create a new empty invoice", () => {

    const invoice = new Invoice();
    expect(invoice.invoiceDate).toBeInstanceOf(Date);
    expect(invoice.invoiceNumber).toBe("");
    expect(invoice.lineItems).toBeInstanceOf(Array);
    expect(invoice.lineItems.length).toBe(0);

  });

  it("should create a new empty invoice with an invoice number", () => {
    const invoice = new Invoice(new Date(), "XXX1234");
    expect(invoice.invoiceNumber).toBe("XXX1234");
  });

  it("should add invoice lines and calculate the total", () => {
    const invoice = new Invoice();
    invoice.addInvoiceLine(new InvoiceLine(1, 10.21, 4, "Banana"));
    invoice.addInvoiceLine(new InvoiceLine(2, 5.21, 1, "Orange"));
    invoice.addInvoiceLine(new InvoiceLine(3, 6.21, 5, "Pineapple"));

    expect(invoice.lineItems.length).toBe(3);
    expect(invoice.getTotal()).toBe(10.21 * 4 + 5.21 + 6.21 * 5);
  });

  it("should remove an invoice by the id", () => {
    const invoice = new Invoice();

    invoice.addInvoiceLine(new InvoiceLine(1, 10.21, 1, "Orange"));
    invoice.addInvoiceLine(new InvoiceLine(2, 10.99, 5, "Banana"));

    invoice.removeInvoiceLine(1);

    expect(invoice.lineItems.length).toBe(1);
    expect(invoice.getTotal()).toBe(10.99 * 5);
  });

  it("should merge two invoices", () => {
    const invoice1 = new Invoice();

    invoice1.addInvoiceLine(new InvoiceLine(1, 10.21, 1, "Blueberries"));

    const invoice2 = new Invoice();

    invoice2.addInvoiceLine(new InvoiceLine(2, 5.29, 4, "Orange"));
    invoice2.addInvoiceLine(new InvoiceLine(3, 9.99, 1, "Banana"));

    invoice1.mergeInvoices(invoice2);

    expect(invoice1.lineItems.length).toBe(3);
    expect(invoice1.getTotal()).toBe(10.21 * 1 + 5.29 * 4 + 9.99 * 1);
  });

  it("should clone and create a new instance", () => {
    const invoice = new Invoice();

    invoice.addInvoiceLine(new InvoiceLine(1, 0.99, 5, "Onion"));
    invoice.addInvoiceLine(new InvoiceLine(2, 10.49, 2, "Watermelon"));

    const clonedInvoice = invoice.clone();

    expect(clonedInvoice).not.toBe(invoice);
  });

  it("should clone the invoice items", () => {
    const invoice = new Invoice();

    invoice.addInvoiceLine(new InvoiceLine(1, 0.99, 5, "Onion"));
    invoice.addInvoiceLine(new InvoiceLine(2, 10.49, 2, "Watermelon"));

    const clonedInvoice = invoice.clone();

    expect(clonedInvoice.lineItems.length).toBe(2);
    expect(clonedInvoice.getTotal()).toBe(invoice.getTotal());
  });

  it("should clone the invoice date and number", () => {
    const invoice = new Invoice(new Date(), "XXX1234");
    const clonedInvoice = invoice.clone();

    expect(clonedInvoice.invoiceNumber).toBe("XXX1234");
    expect(clonedInvoice.invoiceDate.getTime()).toBe(invoice.invoiceDate.getTime());
  });

});
