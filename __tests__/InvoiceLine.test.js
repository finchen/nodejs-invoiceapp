"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const invoiceLine_1 = require("../src/models/invoiceLine");
describe("invoice line class", () => {
    it("should create an invoice line", () => {
        const invoiceLine = new invoiceLine_1.default(1, 6.99, 1, "Apple");
        expect(invoiceLine.invoiceLineId).toBe(1);
        expect(invoiceLine.cost).toBe(6.99);
        expect(invoiceLine.quantity).toBe(1);
        expect(invoiceLine.description).toBe("Apple");
    });
    it("should change the description", () => {
        const invoiceLine = new invoiceLine_1.default(1, 6.99, 1, "Apple");
        invoiceLine.description = "Rotten Apple";
        expect(invoiceLine.description).toBe("Rotten Apple");
    });
});
//# sourceMappingURL=InvoiceLine.test.js.map